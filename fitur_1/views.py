from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from datetime import datetime



# Create your views here.


def landing_page(request):
    return render(request, "index.html")

def login(request):
    if(request.method== "POST"):
        ktp = request.POST['ktp']
        email = request.POST['email']
        cursor=connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("SELECT * from person where ktp='"+ktp+"' and email='"+email+"'")
        person=cursor.fetchone();
        if (person):
            cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
            petugas = cursor.fetchone();
            if(petugas):
                cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
                nama = cursor.fetchall()
                for row in nama:
                    nama = row[0]
                request.session['nama'] = nama
                request.session['ktp'] = ktp
                request.session['email'] = email
                request.session['role'] = 'petugas'
                return redirect('fitur_2:Penugasan')
            else:
                cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
                anggota = cursor.fetchone()
                if(anggota):
                    cursor.execute("SELECT nama from person where ktp='"+ktp+"'")
                    nama = cursor.fetchall()
                    for row in nama:
                        nama = row[0]
                    request.session['nama'] = nama
                    request.session['ktp'] = ktp
                    request.session['email'] = email
                    request.session['role'] = 'anggota'
                    return redirect('fitur_3:daftar_sepeda')
                else:
                    messages.error(request, "email atau ktp salah")
                    return redirect('fitur_1:signin')
        else:
            messages.error(request, "email atau ktp salah")
            return redirect('fitur_1:signin')
    else:
        if ('nama' in request.session.keys()):
            return redirect('/')
        return render(request, "login.html")

def logout(request):
    request.session.flush()
    return redirect('fitur_1:signout')

def register(request):
    if(request.method== "POST"):
        ktp = str(request.POST['ktp'])
        nama= request.POST['nama']
        email= request.POST['email']
        telepon = str(request.POST['telepon'])
        tanggal =request.POST['tanggal']
        alamat = request.POST['alamat']
        
        if(request.POST['role'] == 'anggota'):
            points = '0'
            saldo = '0'
            no_kartu = generate_no_kartu()
            if(no_kartu == -1):
                messages.error(request, "Register Gagal: Jumlah anggota sudah melebihi maksimal")
                return redirect('fitur_1:signup')
            cursor=connection.cursor()
            cursor.execute('set search_path to bikesharing')
            cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
            ktpExist=cursor.fetchone();
            if (ktpExist):
                if(len(ktpExist) > 0):
                    messages.error(request, "Register Gagal: KTP "+ktp+" sudah pernah diregistrasi")
                    return redirect('fitur_1:signup')

            cursor=connection.cursor()
            cursor.execute('set search_path to bikesharing')
            cursor.execute("SELECT * from person where email='"+email+"'")
            emailExist=cursor.fetchone();
            if (emailExist):
                if(len(emailExist) > 0):
                    messages.error(request, "Register Gagal: Email "+email+" sudah pernah diregistrasi")
                    return redirect('fitur_1:signup')
            
            # Kalo semua sudah sukses
            values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+telepon+"'"
            query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values ("+ values +")"  
            cursor.execute(query)

            values = "'"+no_kartu+"',"+ saldo + "," + points +"," + "'"+ktp+"'"
            query = "INSERT INTO anggota (no_kartu,saldo,points,ktp) values (" + values + ")"
            cursor.execute(query)
            
            messages.success(request, "Register Berhasil: "+ nama)
            return redirect('fitur_1:signin')

        elif(request.POST['role'] == 'petugas'):
            gaji='30000'
            cursor=connection.cursor()
            cursor.execute('set search_path to bikesharing')
            cursor.execute("SELECT * from petugas where ktp='"+ktp+"'")
            ktpExist=cursor.fetchone();
            if (ktpExist):
                if(len(ktpExist) > 0):
                    messages.error(request, "Register Gagal: KTP "+ktp+" sudah pernah diregistrasi")
                    return redirect('fitur_1:signup')

            cursor=connection.cursor()
            cursor.execute('set search_path to bikesharing')
            cursor.execute("SELECT * from person where email='"+email+"'")
            emailExist=cursor.fetchone();
            if (emailExist):
                if(len(emailExist) > 0):
                    messages.error(request, "Register Gagal: Email "+email+" sudah pernah diregistrasi")
                    return redirect('fitur_1:signup')
            
            # Kalo semua sudah sukses
            values = "'"+ktp+"','"+email+"','"+nama+"','"+alamat+"','"+tanggal+"','"+telepon+"'"
            query = "INSERT INTO person (ktp,email,nama,alamat,tgl_lahir,no_telp) values ("+ values +")"  
            cursor.execute(query)

            values = "'"+ktp+"',"+ gaji
            query = "INSERT INTO petugas (ktp, gaji) values (" + values+ ")"
            cursor.execute(query)
            messages.success(request, "Register Berhasil: "+ nama)
            return redirect('fitur_1:signin')

        else:
            messages.error(request, "kamu belum memilih role")
            return redirect('fitur_1:signup')
    else:
        if ('nama' in request.session.keys()):
            return redirect('/')
        return render(request, "register.html")

kartu = 46 # karena di database terakhir sampai 46
def generate_no_kartu():
    global kartu
    kartu += 1 
    if(kartu> 9999):
        return -1
    strKartu = "MEMBER" + str(("%04d"%kartu)[-4:])
    cursor=connection.cursor()
    cursor.execute('set search_path to bikesharing')
    cursor.execute("SELECT * from anggota where no_kartu='"+strKartu+"'")
    hasil=cursor.fetchone();
    if (hasil):
        if len(hasil)>0:
            generate_no_kartu()
    return strKartu


def riwayat(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'anggota'):
            cursor = connection.cursor()
            cursor.execute('set search_path to bikesharing,public')
            response={}
            ktp = request.session['ktp']
            cursor.execute("select t.date_time,t.jenis,t.nominal from anggota as a, transaksi as t where a.ktp='"+
                ktp+
                "' and t.no_kartu_anggota = a.no_kartu;")    
            hasil1 = cursor.fetchall()
            response['transaksi'] = hasil1
            return render (request, 'riwayat.html',response)
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def topup(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'anggota'):
            if(request.method == 'POST'):
                cursor = connection.cursor()
                cursor.execute('set search_path to bikesharing,public')
                inputSaldo = request.POST['topup']    
                ktp = request.session['ktp']
                cursor.execute("select saldo from anggota  where ktp ='"+ktp+"' ")
                saldo = cursor.fetchall()
                for row in saldo:
                    saldo1 = row[0]
                saldoakhir = int(saldo1) + int(inputSaldo)
                cursor.execute("UPDATE anggota SET saldo = '" + str(saldoakhir) + "' where ktp ='"+ktp+"' ")       

                cursor.execute("SELECT * from anggota where ktp='"+ktp+"'")
                anggota = cursor.fetchone()

                hariIni = datetime.now().strftime("%Y-%m-%d %H:%M:%S")   
                values = "'" + anggota[0] + "','" + hariIni + "','Topup','" + inputSaldo + "'" 
                cursor.execute("INSERT INTO TRANSAKSI (no_kartu_anggota, date_time, jenis, nominal) VALUES (" + values +")")
              
                messages.success(request, "Topup saldo sebesar " + str(inputSaldo) + " sukses")
                return redirect('fitur_1:riwayat')
            else:
                return render(request, "topup.html")
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def laporan(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            cursor = connection.cursor()
            cursor.execute('set search_path to bikesharing,public')
            response={}
            cursor.execute("select n.nama, l.id_laporan,l.no_kartu_anggota, l.datetime_pinjam, l.status, p.denda " +
            "from person as n, laporan as l,  anggota as a, peminjaman as p " + 
            "where  n.ktp = a.ktp and a.no_kartu = l.no_kartu_anggota and l.no_kartu_anggota = p.no_kartu_anggota " +
            "and l.datetime_pinjam= p.datetime_pinjam and l.nomor_sepeda = p.nomor_sepeda;")
            hasil1 = cursor.fetchall()
            response['laporan'] = hasil1
            return render (request, 'laporan.html', response)
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

@csrf_exempt
def saldo(request):
    response ={}
    if(request.method=="POST"):
      cursor = connection.cursor()
      cursor.execute('set search_path to bikesharing,public')
      ktp = request.session['ktp']
      cursor.execute("select saldo from anggota  where ktp ='"+ktp+"' ")
      saldo = cursor.fetchall()
      for row in saldo:
         saldo = row[0]
      response["saldo"] = saldo
      return JsonResponse(response)
    else:
      return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")


