from django.urls import path
from . import views
 

app_name = 'fitur_1'

urlpatterns = [
    path('', views.landing_page, name="landing"),
    path('laporan/', views.laporan, name="laporan"),
    path('transaksi/', views.riwayat, name="riwayat"),
    path('transaksi/topup/', views.topup, name="topup"),
    path('signin/', views.login, name="signin"),
    path('signup/', views.register, name="signup"),
    path('signout/', views.logout, name="signout"),
    path('saldo/', views.saldo, name="saldo"),
]