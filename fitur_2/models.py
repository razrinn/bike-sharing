from django.db import models

class daftar_acara(models.Model):
    id_acara = models.CharField(max_length = 10)
    judul = models.CharField(max_length = 100)
    deskripsi = models.TextField
    tgl_mulai = models.DateTimeField
    tgl_akhir = models.DateTimeField
    is_free = models.BooleanField

