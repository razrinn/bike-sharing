from django.urls import path, include
from django.conf.urls import url
from django.contrib import admin
from . import views

#url for app
app_name = "fitur_2"
urlpatterns = [
    url("TambahAcara/", views.DaftarAcara, name = "Daftar Acara"),
    url("ValueUpdateAcara/", views.ValueUpdateAcara, name = "Value Update Acara"),
    url("ValueAcara/", views.ValueDaftarAcara, name = "Value Acara"),
    url("TambahPenugasan/", views.DaftarPenugasan, name= "Daftar Penugasan"),
    url("ValueUpdatePenugasan/", views.ValueUpdatePenugasan, name = "Value Update Penugasan"),
    url("ValuePenugasan/", views.ValueDaftarPenugasan, name = "Value Penugasan"),
    url("UpdateAcara/", views.UpdateAcara, name = "Update Acara"),
    url("UpdatePenugasan/", views.UpdatePenugasan, name= "Update Penugasan"),
    url("Acara/", views.Acara, name="Acara"),
    url("HapusAcara/", views.HapusAcara, name="Hapus Acara"),
    url("Penugasan/", views.Penugasan, name="Penugasan")
]
