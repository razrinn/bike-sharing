from django.shortcuts import render, redirect
from django.db import connection
from django.http import HttpResponseRedirect
from django.contrib import messages

def DaftarAcara(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            responseAcara = {}
            array = []
            cursor = connection.cursor()
            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select nama, id_stasiun from stasiun')
            hasil = cursor.fetchall()
            for detail in hasil:
                detail = [detail[0], detail[1]]
                array.append(detail)
            responseAcara['acara'] = array
            return render(request, 'DaftarAcara.html', {"responseAcara" : responseAcara['acara']})
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def ValueDaftarAcara(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            if (request.method == 'POST'):
                print('valid')
                cursor=connection.cursor()
                cursor.execute('set search_path to bikesharing,public')
                id_acara = "AC" + str(GenerateIdAcara())
                judul_acara = request.POST['judul_acara']
                deskripsi = request.POST['deskripsi']
                harga_acara = request.POST['harga_acara']
                if(harga_acara == "Ya"):
                    harga_acara = "t"
                else:
                    harga_acara = "f"
                    
                tgl_waktu_mulai = request.POST['tgl_mulai']
                tgl_waktu_akhir = request.POST['tgl_akhir']
                stasiun = str(request.POST['stasiun'])

                queryInsertAcara = "INSERT INTO acara (id_acara,judul,deskripsi,tgl_mulai, tgl_akhir, is_free) values ('{}', '{}', '{}', '{}', '{}', '{}')".format(
                        id_acara, judul_acara, deskripsi, tgl_waktu_mulai, tgl_waktu_akhir, harga_acara) 
                cursor.execute(queryInsertAcara)
                queryInsertAcaraStasiun = "INSERT INTO acara_stasiun (id_stasiun, id_acara) values ('{}', '{}')".format(stasiun, id_acara)
                cursor.execute(queryInsertAcaraStasiun)
                
                return HttpResponseRedirect('/Acara/')
            else:
                return HttpResponseRedirect('/TambahAcara/')
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def DaftarPenugasan(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            responsePenugasan = {}
            arrayStasiun = []
            arrayPetugas = []
            cursor = connection.cursor()
            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select id_stasiun from stasiun')
            hasil = cursor.fetchall()
            for detail in hasil:
                detail = detail[0]
                arrayStasiun.append(detail)
            responsePenugasan['stasiun'] = arrayStasiun

            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select ktp from petugas')
            hasil = cursor.fetchall()
            for detail in hasil:
                detail = detail[0]
                arrayPetugas.append(detail)
            responsePenugasan['petugas'] = arrayPetugas
            return render(request, 'FormulirPenugasan.html', {"responseStasiun" : responsePenugasan['stasiun'], "responsePetugas" : responsePenugasan['petugas']})
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def ValueDaftarPenugasan(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            if (request.method == 'POST'):
                print('valid')
                cursor=connection.cursor()
                cursor.execute('set search_path to bikesharing,public')

                ktp = request.POST['petugas']
                tgl_waktumulai = request.POST['tgl_mulai']
                tgl_waktu_mulai = tgl_waktumulai.split('T')
                tgl_waktu_mulai = tgl_waktu_mulai[0] + " " + tgl_waktu_mulai[1] + ":00"
                tgl_waktuakhir = request.POST['tgl_akhir']
                tgl_waktu_akhir = tgl_waktuakhir.split('T')
                tgl_waktu_akhir = tgl_waktu_akhir[0] + " " + tgl_waktu_akhir[1] + ":00"
                stasiun = request.POST['stasiun']

                queryInsertPenugasan = "INSERT INTO penugasan (ktp, start_datetime, id_stasiun, end_datetime) values ('{}', '{}', '{}', '{}')".format(ktp, tgl_waktu_mulai, stasiun, tgl_waktu_akhir) 
                cursor.execute(queryInsertPenugasan)
                
                return HttpResponseRedirect('/Penugasan/')
            else:
                return HttpResponseRedirect('/TambahPenugasan/')
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def UpdateAcara(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            responseAcara = {}
            id_acara = request.POST['update']
            array = []
            cursor = connection.cursor()
            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select nama, id_stasiun from stasiun')
            hasil = cursor.fetchall()

            for detail in hasil:
                detail = [detail[0], detail[1]]
                array.append(detail)
            responseAcara['acara'] = array
            hasilAcara = getDetailFromOneRowAcara(id_acara)
            responseAcara['update'] = []
            for detail in hasilAcara:
                if (detail == True):
                    detail = "Ya"
                elif (detail == False):
                    detail = "Tidak"
                responseAcara['update'].append(detail)

            queryNamaStasiun = "select nama from stasiun as s, acara as c, acara_stasiun as acs where c.id_acara = '{}' and c.id_acara = acs.id_acara and s.id_stasiun = acs.id_stasiun".format(id_acara)
            cursor.execute(queryNamaStasiun)
            hasilNamaStasiun = cursor.fetchall()
            daftarStasiun = ""
            for namaStasiun in hasilNamaStasiun:
                nama = namaStasiun[0]
                daftarStasiun = daftarStasiun + ", " + nama
            responseAcara['namaStasiun'] = daftarStasiun
            return render(request, 'UpdateAcara.html', {"responseAcara" : responseAcara['acara'], 'responseUpdate' : responseAcara['update'], 'responseNamaStasiun': responseAcara['namaStasiun']})
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def getDetailFromOneRowAcara(id_acara):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    queryUpdateAcara = "select * from acara where id_acara = '{}'".format(id_acara)
    cursor.execute(queryUpdateAcara)
    hasilAcara = cursor.fetchall()
    hasilAcara = hasilAcara[0]
    return hasilAcara

def ValueUpdateAcara(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            if (request.method == 'POST'):
                print('valid')
                cursor=connection.cursor()
                cursor.execute('set search_path to bikesharing,public')

                idAcara = request.POST['id_acara']
                hasilAcara = getDetailFromOneRowAcara(idAcara)

                judul_acara = request.POST['judul_acara']
                if(hasilAcara[1] != judul_acara):
                    queryUpdateJudul = "UPDATE acara SET judul = '{}' where id_acara = '{}'".format(judul_acara, idAcara)
                    cursor.execute(queryUpdateJudul)

                deskripsi = request.POST['deskripsi']
                if(hasilAcara[2] != deskripsi):
                    queryUpdateDeskripsi = "UPDATE acara SET deskripsi = '{}' where id_acara = '{}'".format(deskripsi, idAcara)
                    cursor.execute(queryUpdateDeskripsi)

                harga_acara = request.POST['harga_acara']
                if(harga_acara == "Ya"):
                    harga_acara = "t"
                else:
                    harga_acara = "f"
                
                if((harga_acara == "t" and hasilAcara[5] != True) or (harga_acara == 'f' and hasilAcara != False)):
                    queryUpdateHarga = "UPDATE acara SET is_free = '{}' where id_acara = '{}'".format(harga_acara, idAcara)
                    cursor.execute(queryUpdateHarga)

                tgl_waktu_mulai = request.POST['tgl_mulai']
                queryUpdateTglMulai = "UPDATE acara SET tgl_mulai = '{}' where id_acara = '{}'".format(tgl_waktu_mulai, idAcara)
                cursor.execute(queryUpdateTglMulai)

                
                tgl_waktu_akhir = request.POST['tgl_akhir']
                queryUpdateTglAkhir = "UPDATE acara SET tgl_akhir = '{}' where id_acara = '{}'".format(tgl_waktu_akhir, idAcara)
                cursor.execute(queryUpdateTglAkhir)
                
                stasiun = str(request.POST['stasiun'])

                return HttpResponseRedirect('/Acara/')
            else:
                return HttpResponseRedirect('/UpdateAcara/')
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def UpdatePenugasan(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            responsePenugasan = {}
            id_penugasan = request.POST['update'].split('+')
            id_penugasan[0] = id_penugasan[0][1:-1]
            id_penugasan[1] = id_penugasan[1][1:-1]
            arrayStasiun = []
            arrayPetugas = []
            cursor = connection.cursor()

            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select id_stasiun from stasiun')
            hasil = cursor.fetchall()
            for detail in hasil:
                detail = detail[0]
                arrayStasiun.append(detail)
            responsePenugasan['stasiun'] = arrayStasiun

            cursor.execute('set search_path to bikesharing,public')
            cursor.execute('select ktp from petugas')
            hasil = cursor.fetchall()
            for detail in hasil:
                detail = detail[0]
                arrayPetugas.append(detail)
            responsePenugasan['petugas'] = arrayPetugas

            hasilPenugasan = getDetailFromOneRowPenugasan(id_penugasan[0], id_penugasan[1])
            responsePenugasan['update'] = []
            for detail in hasilPenugasan:
                responsePenugasan['update'].append(detail)

            return render(request, 'UpdateFormulirPenugasan.html', {"responseStasiun" : responsePenugasan['stasiun'], "responsePetugas" : responsePenugasan['petugas'], "responseUpdate" : responsePenugasan['update']})
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def getDetailFromOneRowPenugasan(ktp, id_stasiun):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    queryUpdatePenugasan = "select * from penugasan where ktp = '{}' and id_stasiun = '{}'".format(ktp, id_stasiun)
    cursor.execute(queryUpdatePenugasan)
    hasilPenugasan = cursor.fetchall()
    hasilPenugasan = hasilPenugasan[0]
    return hasilPenugasan

def ValueUpdatePenugasan(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            if (request.method == 'POST'):
                print('valid')
                cursor=connection.cursor()
                cursor.execute('set search_path to bikesharing,public')

                id_penugasan = request.POST['update'].split('+')
                id_penugasan[0] = id_penugasan[0][1:-1]
                id_penugasan[1] = id_penugasan[1][1:-1]
                hasilPenugasan = getDetailFromOneRowPenugasan(id_penugasan[0], id_penugasan[1])

                ktp = request.POST['petugas']
                tgl_waktumulai = request.POST['tgl_mulai']
                tgl_waktu_mulai = tgl_waktumulai.split('T')
                tgl_waktu_mulai = tgl_waktu_mulai[0] + " " + tgl_waktu_mulai[1] + ":00"
                tgl_waktuakhir = request.POST['tgl_akhir']
                tgl_waktu_akhir = tgl_waktuakhir.split('T')
                tgl_waktu_akhir = tgl_waktu_akhir[0] + " " + tgl_waktu_akhir[1] + ":00"
                stasiun = request.POST['stasiun']

                if(hasilPenugasan[1] != tgl_waktu_mulai):
                    queryUpdateWaktuMulai = "UPDATE penugasan SET start_datetime = '{}' where ktp = '{}'".format(tgl_waktu_mulai, ktp)
                    cursor.execute(queryUpdateWaktuMulai)
                if(hasilPenugasan[2] != tgl_waktu_akhir):
                    queryUpdateWaktuAkhir = "UPDATE penugasan SET end_datetime = '{}' where ktp = '{}'".format(tgl_waktu_akhir, ktp)
                    cursor.execute(queryUpdateWaktuAkhir)

                queryUpdateStasiun = "UPDATE penugasan SET id_stasiun = '{}' where ktp = '{}'".format(stasiun, ktp)
                cursor.execute(queryUpdateStasiun)
                return HttpResponseRedirect('/Penugasan/')
            else:
                return HttpResponseRedirect('/UpdatePenugasan/')
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def Acara(request): 
    user_type = "admin"
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    responseDaftarAcara={}
    cursor.execute("select * " + 
    "from acara;")  
    hasil = cursor.fetchall()
    responseDaftarAcara['acara'] = hasil
    return render(request, 'TampilkanAcara.html', {
        "responseDaftarAcara" : responseDaftarAcara['acara'],
        "type" : user_type
    })

def HapusAcara(request):
    if('role' in request.session.keys()):
        if(request.session['role'] == 'petugas'):
            if (request.method == 'POST'):
                print('sukses')
                response = request.POST['delete']
                print(request.POST['delete'])
                return HttpResponseRedirect('/Acara/')
            else:
                print('anehhh')
                return HttpResponseRedirect('/TambahAcara/')
        else:
            messages.error(request, "Role tidak diizinkan melihat halaman tersebut")   
            return redirect('fitur_1:landing')
    else:
        messages.error(request, "Silahkan login terlebih dahulu")
        return redirect('fitur_1:signin')

def Penugasan(request):
    user_type = "admin"
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    responseDaftarPenugasan={}
    cursor.execute("select p.ktp, p.start_datetime, p.end_datetime, p.id_stasiun " + 
    "from penugasan as p, petugas as pe, stasiun as s " +
    "where p.ktp = pe.ktp and s.id_stasiun = p.id_stasiun;")  
    hasil = cursor.fetchall()
    responseDaftarPenugasan['penugasan'] = hasil
    return render(request, 'TampilkanPenugasan.html', {
        "responseDaftarPenugasan" : responseDaftarPenugasan['penugasan'],
        "type" : user_type
    })

def GenerateIdAcara():
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute('select id_acara from acara order by id_acara DESC limit 1')
    hasilIdAcara = cursor.fetchall()
    array = []
    for detail in hasilIdAcara:
        detail = detail[0]
        array.append(detail[2:])
    id_baru = int(array[0]) + 1
    return id_baru