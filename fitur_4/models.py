from django.db import models

# Create your models here.
class voucher(models.Model):
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    point = models.CharField(max_length=30)
    description = models.CharField(max_length=30)
    amount = models.CharField(max_length=30)