from django import forms

class createVoucher(forms.Form):

    invalid_message={
        'required' : 'Please fill out this field',
        'invalid' : 'Please fill out with valid Input',
    }

    name_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter your name',
        'cols' : 10,
    }

    category_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter the category',
    }

    point_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter the voucher point',
    }

    description_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please describe the voucher',
    }

    amount_attrs={
        'type' : 'text',
        'class' : 'todo-form-input',
        'placeholder' : 'Please enter the amount of the voucher',
    }

    name = forms.CharField(label="Nama", required=True, max_length=30, widget=forms.TextInput(attrs = name_attrs))
    category = forms.CharField(label="Kategori", required=True, max_length=30, widget=forms.TextInput(attrs = category_attrs))
    point = forms.CharField(label="Jumlah poin", required=True, max_length=30, widget=forms.TextInput(attrs = point_attrs))
    description = forms.CharField(label="Deskripsi", required=True, widget=forms.TextInput(attrs = description_attrs))
    amount = forms.CharField(label="Jumlah voucher", required=True, max_length=30, widget=forms.TextInput(attrs = amount_attrs))