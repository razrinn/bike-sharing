from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from .models import voucher
from .forms import createVoucher
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
def create_voucher_fitur_4(request):
    if(request.session['role'] == 'petugas'):
        cursor=connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("SELECT kategori FROM VOUCHER")
        result1 = cursor.fetchall()
        response['voucher'] = result1
        print(response)
        return render(request, 'create_voucher.html', response)
    else:
        return HttpResponseRedirect('/')

def daftar_voucher_fitur_4(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute("select * from voucher")
    result1 = cursor.fetchall()
    response['voucher'] = result1
    print(response)
    return render(request, 'daftar_voucher.html', response)

    # form = createVoucher(request.POST or None)
    # response['form'] = form
    # return render(request, 'daftar_voucher.html', response)

# def update_voucher_fitur_4(request):
#     form = createVoucher(request.POST or None)
#     response['form'] = form
#     return render(request, 'update_voucher.html', response)

def postVoucher(request):
    if (request.method == "POST"):
        nama = request.POST['nama']
        kategori= request.POST['kategori']
        poin = request.POST['nilai_poin']
        deskripsi = request.POST['deskripsi']
        jumlah = request.POST['jumlah']
        ktp_petugas = request.session['ktp']
        print(ktp_petugas)
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("select id_voucher from voucher order by cast (id_voucher as int) desc;")
        hasil = cursor.fetchone()
        id_voucher = int(hasil[0]) + 1


        print(id_voucher)
        cursor.execute("INSERT INTO voucher(id_voucher, nama, kategori, nilai_poin, deskripsi, jumlah) values(" + "'" + str(
            id_voucher) + "','" + str(nama) + "','" + kategori + "'," + poin + ",'" + deskripsi + "','" + jumlah
            + "');")
        messages.error(request, "success")
        return HttpResponseRedirect('/fitur_4/daftar_voucher')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/fitur_4/create_voucher')


@csrf_exempt
def deleteVoucher(request):
    if (request.method == "POST"):

        id_voucher = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("delete from voucher where id_voucher='" + id_voucher + "';")
        messages.error(request, "success")
        return HttpResponseRedirect('/fitur_4/daftar_voucher')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/fitur_4/daftar_voucher')


@csrf_exempt
def updateVoucher(request):
    if (request.method == "POST"):
        id_voucher = request.POST['id_voucher']
        nama = request.POST['nama']
        kategori = request.POST['kategori']
        poin = request.POST['poin']
        deskripsi = request.POST['deskripsi']
        jumlah = request.POST['jumlah']
        print(jumlah)
        cursor = connection.cursor()
        cursor.execute('set search_path to bikesharing,public')
        cursor.execute("update voucher set nama='" + nama + "', kategori='" +
            kategori + "', nilai_poin='" + poin + "', deskripsi='" + deskripsi + "', jumlah='" + jumlah + "' where id_voucher = '" + id_voucher + "';")
        messages.error(request, "success")
        return HttpResponseRedirect('/fitur_4/daftar_voucher')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/fitur_4/daftar_voucher')


# @csrf_exempt
# def detailUpdateVoucher(request):
#     if (request.method == "POST"):
#         response = {}

#         voucher = request.POST['id']
#         cursor = connection.cursor()
#         cursor.execute('set search_path to bikesharing,public')
#         cursor.execute("select * from voucher where id_voucher='" + voucher + "';")
#         id_voucher = cursor.fetchone()
#         cursor.execute("select kategori from voucher")
#         kategori = cursor.fetchall()
#         # cursor.execute(
#         #     "select s.nama from stasiun as s, acara_stasiun as a where a.id_acara ='" + acara + "' and a.id_stasiun = s.id_stasiun;")
#         # nama_stasiun = cursor.fetchall()
#         response['kategori'] = kategori
#         response['voucher'] = id_voucher
#         # response['nama_stasiun'] = nama_stasiun
#         messages.error(request, "success")
#         return JsonResponse(response)

#     else:
#         messages.error(request, "gagal")
#         return HttpResponseRedirect('/fitur_4/daftar_voucher')


def create_pinjaman_fitur_4 (request):
    cursor = connection.cursor()
    cursor.execute("SET search_path to bikesharing,public")
    # cursor.execute("SELECT * FROM sepeda WHERE status ='f'")
    cursor.execute("select sp.merk, st.nama from sepeda as sp, stasiun as st where sp.id_stasiun = st.id_stasiun")
    nama_sepeda = cursor.fetchall()
    response['nama_sepeda'] = nama_sepeda
    print(response)
    return render(request, 'create_pinjaman.html', response)

    
def pinjem_sepeda(request):
    if (request.method == "POST"):
        nama_sepeda = request.POST['nama_sepeda']
        temp = nama_sepeda.split(" ")
        merk = temp[0]
        stasiun = temp[1]
        ktp = request.session['ktp']
        print(stasiun)
        cursor = connection.cursor()
        cursor.execute("SET search_path to bikesharing,public")
        cursor.execute("select no_kartu from anggota where ktp = '" + str(ktp)+"';")
        no_kartu_anggota = cursor.fetchone()
        no_kartu_anggota = no_kartu_anggota[0]
        cursor.execute("select nomor from sepeda where merk = '" + merk + "';")
        nomor_sepeda = cursor.fetchone()
        nomor_sepeda = nomor_sepeda[0]
        print(nomor_sepeda)
        cursor.execute("select id_stasiun from stasiun where nama = '" + stasiun + "';")
        id_stasiun = cursor.fetchone()
        id_stasiun = id_stasiun[0]
        cursor.execute("set timezone = 'asia/jakarta'")
        cursor.execute("INSERT INTO peminjaman(no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun) values(" + "'" + no_kartu_anggota + "', now()::timestamp, " +
                        "'" + nomor_sepeda + "', " + "'" + id_stasiun + "');")
        messages.error(request, "success")
        return render(request, 'daftar_pinjaman.html', response)
    else:
        messages.error(request, "Peminjaman gagal")
        return render(request, 'daftar_pinjaman.html', response)

def daftar_pinjaman_fitur_4 (request):
    # ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute('select p.no_kartu_anggota, p.datetime_pinjam, sp.merk, st.nama, p.datetime_kembali, p.biaya, p.denda from peminjaman as p, sepeda as sp, stasiun as st where p.nomor_sepeda = sp.nomor and p.id_stasiun = st.id_stasiun;')
    pinjaman = cursor.fetchall()
    response['pinjaman'] = pinjaman
    return render(request, 'daftar_pinjaman.html', response)


def daftarPinjaman(request):
    ktp = request.session['ktp']
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing,public')
    cursor.execute('select p.no_kartu_anggota, p.datetime_pinjam, sp.merk, st.nama, p.datetime_kembali, p.biaya, p.denda from peminjaman as p, sepeda as sp, stasiun as st where p.nomor_sepeda = sp.nomor and p.id_stasiun = st.id_stasiun;')
    allPinjaman = cursor.fetchall()
    allPinjaman = Paginator(allLaporan, 5) # Show 5 contacts per page
    print(allPinjaman)
    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request, 'daftar_pinjaman.html', {'all_posts': contacts})




    