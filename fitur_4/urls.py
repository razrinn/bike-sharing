from django.contrib import admin
from django.urls import path
from . import views

app_name = 'fitur_4'

urlpatterns = [
    path('create_voucher/', views.create_voucher_fitur_4, name="create_voucher"),
    path('daftar_voucher/', views.daftar_voucher_fitur_4, name="daftar_voucher"),
    path('create_pinjaman/', views.create_pinjaman_fitur_4, name="create_pinjaman"),
    path('daftar_pinjaman/', views.daftar_pinjaman_fitur_4, name="daftar_pinjaman"),
]



