from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.

def tambah_stasiun(request):
    return render(request, "tambah-stasiun.html")

def tambah_sepeda(request):
    return render(request, "tambah-sepeda.html")

def update_stasiun(request):
    return render(request, "update-stasiun.html")

def update_sepeda(request):
    return render(request, "update-sepeda.html")

def daftar_stasiun(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select nama, alamat, lat, long, id_stasiun " +
                   "from stasiun;")
    data = cursor.fetchall()
    response['stasiun'] = data
    return render(request, "daftar-stasiun.html", response)

def stasiun_sortby_nama(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select nama, alamat, lat, long, id_stasiun " +
                   "from stasiun " + 
                   "order by nama ASC;")
    data = cursor.fetchall()
    response['stasiun'] = data
    return render(request, "daftar-stasiun.html", response)

def stasiun_sortby_alamat(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select nama, alamat, lat, long, id_stasiun " +
                   "from stasiun " + 
                   "order by alamat ASC;")
    data = cursor.fetchall()
    response['stasiun'] = data
    return render(request, "daftar-stasiun.html", response)

def daftar_sepeda(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp;")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)
    
def sepeda_tersedia(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp " + 
		           "and se.status = 't'; ")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)

def sepeda_sortby_nomor(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp " + 
		           "order by se.nomor ASC; ")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)

def sepeda_sortby_jenis(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp " + 
		           "order by se.jenis ASC; ")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)

def sepeda_sortby_stasiun(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp " + 
		           "order by st.nama ASC; ")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)

def sepeda_sortby_penyumbang(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bikesharing, public')
    response = {}
    cursor.execute("select se.nomor, se.merk, se.jenis, st.nama, se.status, p.nama " +
                   "from sepeda se, stasiun st, person p, anggota a " + 
                   "where se.id_stasiun = st.id_stasiun " +
                   "and se.no_kartu_penyumbang = a.no_kartu " + 
                   "and a.ktp = p.ktp " + 
		           "order by p.nama ASC; ")
    data = cursor.fetchall()
    response['sepeda'] = data
    return render(request, "daftar-sepeda.html", response)

