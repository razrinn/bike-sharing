from django.urls import path
from . import views
 

app_name = "fitur_3"
urlpatterns = [
    path('tambah-stasiun', views.tambah_stasiun, name="tambah_stasiun"),
    path('tambah-sepeda', views.tambah_sepeda, name="tambah_sepeda"),
    path('update-stasiun', views.update_stasiun, name="update_stasiun"),
    path('update-sepeda', views.update_sepeda, name="update_sepeda"),
    path('daftar-stasiun', views.daftar_stasiun, name="daftar_stasiun"),
    path('daftar-stasiun/sorted-nama', views.stasiun_sortby_nama, name="stasiun_sortby_nama"),
    path('daftar-stasiun/sorted-alamat', views.stasiun_sortby_alamat, name="stasiun_sortby_alamat"),
    path('daftar-sepeda', views.daftar_sepeda, name="daftar_sepeda"),
    path('daftar-sepeda/tersedia', views.sepeda_tersedia, name="sepeda_tersedia"),
    path('daftar-sepeda/sorted-nomor', views.sepeda_sortby_nomor, name="sepeda_sortby_nomor"),
    path('daftar-sepeda/sorted-jenis', views.sepeda_sortby_jenis, name="sepeda_sortby_jenis"),
    path('daftar-sepeda/sorted-stasiun', views.sepeda_sortby_stasiun, name="sepeda_sortby_stasiun"),
    path('daftar-sepeda/sorted-penyumbang', views.sepeda_sortby_penyumbang, name="sepeda_sortby_penyumbang"),
]