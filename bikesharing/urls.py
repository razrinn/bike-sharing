"""bikesharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
import fitur_2.urls as fitur_2

# individual views
from fitur_1 import views as fitur_1
from fitur_3 import views as fitur_3

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', fitur_1.landing_page),
    path('fitur_4/', include('fitur_4.urls')),
    # path('', include("fitur_1.urls")),
    path('__admin/', admin.site.urls),
    # path('', fitur_1.landing_page),
    path('', include(fitur_2,namespace='fitur_2')),
    path('', include("fitur_1.urls")),
    path('', include(fitur_2,namespace='fitur_2')),
    # path('', include("fitur_1.urls")),
    path('', include('fitur_3.urls')),
    # path('daftar_stasiun/', fitur_3.daftar_stasiun),
    # path('daftar_sepeda/', fitur_3.daftar_sepeda),
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
