# Tugas Basis Data Kelas A

## Link Heroku

https://bike-sharing-id.herokuapp.com/

## Anggota Kelompok dan Pembagian Tugas:

Tugas Kelompok Basis Data:

1. Irene Pixelyne - 17060XXXXX (No.4)
2. Karina Ivana - 1706979335 (No.2)
3. Nur Nisrina Ningrum - 1706979423 (No.3)
4. Ray Azrin Karim - 1706044111 (No.1)

`* Nomor yang dimaksud adalah nomor pada list halaman yang perlu dikerjakan pada Panduan Tugas Kelompok 4`

## pipeline

[![pipeline status](https://gitlab.com/ppw-d-6/ppw-tp1/badges/master/pipeline.svg)](https://gitlab.com/ppw-d-6/ppw-tp1/commits/master)
